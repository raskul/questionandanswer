<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/home', 'HomeController@index');
//////////////////
Route::get('/', 'IndexController@index')->name('home');

Route::resource('tag', 'TagController');
Route::resource('category', 'CategoryController');
Route::resource('question', 'QuestionController');
Route::resource('answer', 'AnswerController');
Route::resource('comment', 'CommentController');

Route::resource('user', 'UserController');

Route::patch('answer/{answer}/votePositive', 'AnswerController@votePositive')->name('answer.positive');
Route::patch('answer/{answer}/voteNegative', 'AnswerController@voteNegative')->name('answer.negative');
Route::patch('question/{question}/votePositive', 'QuestionController@votePositive')->name('question.positive');// not completed
Route::patch('question/{question}/voteNegative', 'QuestionController@voteNegative')->name('question.negative');// not completed

Route::patch('answer/{answer}/bestAnswer', 'AnswerController@bestAnswer')->name('answer.best');


