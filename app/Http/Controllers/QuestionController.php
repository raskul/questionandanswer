<?php

namespace App\Http\Controllers;

use App\Category;
use App\Question;
use App\Tag;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('throttle:1,0.1')->only('voteNegative', 'votePositive');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::all();

        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        $tags = Tag::pluck('name', 'id');

        return view('question.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        if (auth()->check()) {
            $inputs['name'] = auth()->user()->name;
            $inputs['user_id'] = auth()->user()->id;
        } else $inputs['name'] = $inputs['name'] . ' (کاربر مهمان) ';

        $question = Question::create($inputs);
        $question->tags()->sync($inputs['tag_id']);

        return redirect()->back();

        ///////////////add instant new tag
//        $inputs = $request->all();
//        dd($inputs);
//        $tags[] = $inputs['tag_id'];
////        dd($tags);
//        $i = 0 ;
//        foreach ($tags as $key => $tag){
//            $question = Tag::create(['name' => "$tag[$i]", 'user_id' => '1']);
////            dd($tag[$i]);
//            $inputs['tag_id'] = Tag::where('name', "$tag[$i]")->pluck('id');
//            $i++;
//        }
//        $question = Question::create($inputs);
//        $question->tags()->sync($inputs['tag_id']);
//        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {

        return view('question.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $categories = Category::pluck('name', 'id');
        $tags = Tag::pluck('name', 'id');

        return view('question.edit', compact('question', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $this->authorize('edit-question', $question);

        $inputs = $request->all();

        $question->update($inputs);
        $question->tags()->sync($inputs['tag_id']);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $this->authorize('is-admin');

        $question->delete();

        return redirect()->back();
    }

    public function votePositive(Question $question)
    {
        $question->update(['vote' => $question['vote'] + 1]);
        if (count($question['user_id'])) {
            $question_vote = $question->user()->pluck('vote');
            $question->user()->update(['vote' => $question_vote[0] + 1]);
        }

        return redirect()->back();
    }

    public function voteNegative(Question $question)
    {
        $question->update(['vote' => $question['vote'] - 1]);
        if (count($question['user_id'])) {
            $question_vote = $question->user()->pluck('vote');
            $question->user()->update(['vote' => $question_vote[0] - 1]);
        }

        return redirect()->back();
    }
}
