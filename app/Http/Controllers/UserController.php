<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $path = 'images/profiles/';

    public function index()
    {
        $this->authorize('is-admin');

        $users = User::get();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('is-admin');

        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('is-admin');

        $inputs = $request->all();
        $picname = $request->file('picture')->getClientOriginalName();
        $request->picture->move($this->path, $picname);
        $inputs['picture'] = $this->path . $picname;
        $inputs['password'] = bcrypt($inputs['password']);

        User::create($inputs);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('edit-user', $user);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->authorize('edit-user', $user);

        $inputs = $request->all();
        if($request['picture']){
            $picname = $request->file('picture')->getClientOriginalName();
            $request->picture->move($this->path, $picname);
            $inputs['picture'] = $this->path . $picname;

        }
        else
            unset($inputs['picture']);

        $inputs['password'] = bcrypt($inputs['password']);
        $user->update($inputs);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('is-admin');

        $user->delete();

        return redirect()->back();
    }
}
