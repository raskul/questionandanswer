<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Gate;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('throttle:1,0.1')->only('voteNegative', 'votePositive');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = Answer::all();

        return view('answer.index', compact('answers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('answer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        if (auth()->check()) {
            $inputs['name'] = auth()->user()->name;
            $inputs['user_id'] = auth()->user()->id;
        } else $inputs['name'] = $inputs['name'] . ' (کاربر مهمان) ';

        Answer::create($inputs);

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        return view('answer.show', compact('answer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        return view('answer.edit', compact('answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        $this->authorize('edit-answer', $answer);

        $inputs = $request->all();

        $answer->update($inputs);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        $this->authorize('is-admin');

        $answer->delete();

        return redirect()->back();
    }

    public function votePositive(Answer $answer)
    {
        $answer->update(['vote' => $answer['vote'] + 1]);
        if (count($answer['user_id'])) {
            $answer_vote = $answer->user()->pluck('vote');
            $answer->user()->update(['vote' => $answer_vote[0] + 1]);
        }

        return redirect()->back();
    }

    public function voteNegative(Answer $answer)
    {
        $answer->update(['vote' => $answer['vote'] - 1]);
        if (count($answer['user_id'])){
            $answer_vote = $answer->user()->pluck('vote');
            $answer->user()->update(['vote' => $answer_vote[0] - 1]);
        }

        return redirect()->back();
    }

    public function bestAnswer(Answer $answer)
    {
        $answer['is_best'] = 1;
        $answer->save();
        $question = $answer->question();
        $question->update(['closed' => 1]);
//        $answer->update(['id_best' => 1]);//chera in kar namikone vali khate balaee kar mikone
        return redirect()->back();
    }
}
