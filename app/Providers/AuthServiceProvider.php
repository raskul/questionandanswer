<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('edit-question', function ($user, $question){
            if( ($user->id == $question->user_id) || ($user->admin == 1) )
                return true;
        });

        Gate::define('edit-answer', function ($user, $answer){
            if( ($user->id == $answer->user_id) || ($user->admin == 1) )
                return true;
        });

        Gate::define('edit-comment', function ($user, $comment){
            if( ($user->id == $comment->user_id) || ($user->admin == 1) )
                return true;
        });

        Gate::define('edit-user', function ($user, $x){
            if( ($user->id == $x->id) || ($user->admin == 1) )
                return true;
        });

        Gate::define('is-admin', function ($user){
            if($user->admin == 1)
                return true;
        });

    }
}
