<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name','user_id',
    ];

    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
