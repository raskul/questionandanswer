@extends('layouts.layout')
@section('content')

    @if(Auth::check())
        <div>
                            <span>
                                وارد شده با نام کاربری:
                            </span>
            <span>{{ auth()->user()->name }}</span>
            <span>/</span>
            <span>
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            خروج
                            </a>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            </span>
        </div>
        {!! Form::open(['route'=>['question.store']]) !!}
    @else
    {!! Form::open(['route'=>['question.store']]) !!}

    <!-- فیلد نام شما: -->
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام شما:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>
    <span>
                                <a href="{{ route('register') }}">
                                ثبت نام
                                </a>
                        </span>
    <span>/</span>
    <span>
                                <a href="{{ route('login') }}">
                                ورود
                                </a>
                        </span>
    @endif

    @include('question.fields')

    {!! Form::close() !!}

@endsection
