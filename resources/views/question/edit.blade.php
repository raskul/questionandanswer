@extends('layouts.layout')
@section('content')

    {!! Form::model($question, ['route'=>['question.update', $question->id ], 'method' => 'patch', 'files' => true ]) !!}

    <!-- فیلد نام شما: -->
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام شما:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @if ($errors->has('name'))
            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
    </div>

    @include('question.fields')

    {!! Form::close() !!}
	
@endsection
