@extends('layouts.layout')
@section('content')

    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">{{--for left menu change it to 10--}}

                {{--start question--}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row question-row">
                            <div class="col-sm-1">
                                <span class="">
      سوال:
                                </span>
                            </div>
                            <div class="col-sm-11">
                                <h2 style="display: inline">{{ $question->title }}</h2>

                            </div>
                        </div>
                        <div class="row question">

                            @if(count($question->user_id))
                                <div class="col-sm-1 user">
                                    <div class="row">
                                        <img src="{{ route('home') }}/{{ $question->user->picture }}" alt=""
                                             width="100%" height="100%">
                                    </div>
                                    <div class="row">
                                        نام:
                                        {{ $question->user->name }}
                                    </div>
                                    <div class="row">
                                        امتیاز:
                                        {{ $question->user->vote }}
                                    </div>
                                    <div class="row">
                                        <a href="">
                                            پروفایل
                                        </a>
                                    </div>
                                </div>
                            @else
                                <div class="col-sm-1 user">
                                    <div class="row">
                                        <img src="{{route('home')}}/images/AnonymousMale.jpg" alt="" width="100%"
                                             height="100%">
                                    </div>
                                    <div class="row">
                                            <span>
نام:
                                            </span>
                                        {{ $question->name }}
                                    </div>
                                </div>
                            @endif

                            <div class="col-sm-10 ">
                                <div class="row">
                                    {{ $question->title }}
                                </div>
                                <div class="row">
                                    -
                                </div>
                                <div class="row ">
                                    {!! $question->content !!}
                                    <br>
                                    @can('edit-question', $question)
                                        <span class="edit"><a
                                                    href="{{ route('question.edit', $question->id) }}"> ویرایش </a></span>
                                    @endcan
                                </div>

                                {{--start best answer--}}
                                <div class="row bestanswer">
                                    @foreach($question->answers as $answer2)
                                        @if($answer2->is_best == 1)
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">
                                                        بهترین جواب :
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <span>
                                                            نویسنده :
                                                        </span>
                                                        @if(count($answer2->user_id))
                                                            <span>
                                                                <a href="/profile....">
                                                                {{ $answer2->name }}
                                                                </a>
                                                            </span>
                                                        @else
                                                            <span>
                                                                {{ $answer2->name }}
                                                            </span>
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-11">
                                                            <div class="row">
                                                                {!! $answer2->content !!}
                                                            </div>

                                                            @if(count($answer2->comments))
                                                                <div class="row allcomment">
                                                                    <div class="row">
                                                                        دیدگاه ها:
                                                                    </div>
                                                                    @foreach($answer2->comments as $comment)
                                                                        <div class="comment">
                                                                            {{ $comment->name }}
                                                                            :
                                                                            {{ $comment->content }}
                                                                            @can('edit-comment', $comment)
                                                                                <span class="edit"><a
                                                                                            href="{{ route('comment.edit', $comment->id) }}">ویرایش</a></span>
                                                                            @endcan
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endif
                                                        </div>
                                                        <div class="col-sm-1">
                                                            <div class="row">
                                                                {!! Form::open(['route'=>['answer.positive', $answer2->id ], 'method' => 'patch' ]) !!}
                                                                <button type="submit" class="btn btn-success">
                                                                    <span>
                                                                        &nbspتشکر&nbsp
                                                                    </span>
                                                                </button>
                                                                {!! Form::close() !!}
                                                            </div>
                                                            <div style="margin: 10px">
                                                                {{ $answer2->vote }}
                                                                @if($answer2->vote >= 0) + @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                                {{--end best answer--}}

                                <div class="row" style="margin-top: 60px;margin-right: 20px;margin-bottom: 10px">
                                    <a href="#answerthequestion" class="btn btn-primary">
                                        به این سوال پاسخ دهید
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <div class="row">
                                    {!! Form::open(['route'=>['question.positive', $question->id ], 'method' => 'patch' ]) !!}
                                    <button type="submit" class="btn btn-success">
                                            <span>
                                                &nbspتشکر&nbsp
                                            </span>
                                    </button>
                                    {!! Form::close() !!}
                                </div>
                                <div>
                                    {{ $question->vote }}
                                    @if($question->vote >= 0) + @endif
                                </div>
                                <div class="row">
                                    {!! Form::open(['route'=>['question.negative', $question->id ], 'method' => 'patch' ]) !!}
                                    <button type="submit" class="btn btn-danger">
                                            <span>
                                        گزارش
                                            </span>
                                    </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--end question--}}

                {{--start tags--}}
                <div class="row tag">
                    <div class="col-sm-12">
                        <span>
                            برچسب ها :
                        </span>
                        @if(count($question->tags))
                            @foreach($question->tags as $tag)
                                <a href="{{ route('tag.show', $tag->id) }}">
                                    <span class="btn btn-default btn-xs" style="margin-left: 5px">
                                        {{ $tag->name }}
                                    </span>
                                </a>
                            @endforeach
                        @endif
                        @if(auth()->check())
                            <span>
                            {!! Form::open(['route'=>['tag.store', 'user_id' => auth()->user()->id, 'question_id' => $question->id ] ]) !!}
                            <!-- فیلد افزودن برچسب -->
                                <span class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                {!! Form::label('name', 'افزودن برچسب') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    @if ($errors->has('name'))
                                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                    @endif
                            </span>
                                <button style="display: inline" type="submit" class="btn btn-success">ثبت برچسب</button>
                                {!! Form::close() !!}
                        </span>
                        @endif
                    </div>
                </div>
                {{--end tags--}}

                {{--start sorting--}}
                <div class="row answer-row">
                    <div class="col-sm-12">
                        <span>
                            جواب ها :
                        </span>
                    </div>
                    {{--<div class="col-sm-4">--}}
                    {{--order by--}}
                    {{--</div>--}}
                </div>
                {{--end sorting--}}

                {{--start answers--}}
                @foreach($question->answers as $answer)
                    <div class="row ">
                        <div class="col-sm-12">
                            <div class="row answer">

                                @if(count($answer->user_id))
                                    <div class="col-sm-1 user">
                                        <div class="row">
                                            <img src="{{ route('home') }}/{{ $answer->user->picture }}" alt=""
                                                 width="100%" height="100%">
                                        </div>
                                        <div class="row">
                                            نام:
                                            {{ $answer->user->name }}
                                        </div>
                                        <div class="row">
                                            امتیاز:
                                            {{ $answer->user->vote }}
                                        </div>
                                        <div class="row">
                                            <a href="{{route('user.index')}}/{{ $answer->user->id }}">
                                                پروفایل
                                            </a>
                                        </div>
                                        @if(($answer->is_best) == 1)
                                            <div class="row">
                                                <img src="{{route('home')}}/images/GreenTick.png" alt="" width="100%"
                                                     height="100%">
                                            </div>
                                        @endif
                                    </div>
                                @else
                                    <div class="col-sm-1 user">
                                        <div class="row">
                                            <img src="{{route('home')}}/images/AnonymousMale.jpg" alt="" width="100%"
                                                 height="100%">
                                        </div>
                                        <div class="row">
                                            <span>
نام:
                                            </span>
                                            {{ $answer->name }}
                                        </div>
                                        @if(($answer->is_best) == 1)
                                            <div class="row">
                                                <img src="{{route('home')}}/images/GreenTick.png" alt="" width="100%"
                                                     height="100%">
                                            </div>
                                        @endif
                                    </div>
                                @endif

                                <div class="col-sm-10 ">
                                    @can('edit-question', $question)
                                        @if($question['closed'] == 0)
                                            <div class="row">
                                                {!! Form::open(['route'=>['answer.best', $answer->id ], 'method' => 'patch' ]) !!}
                                                <button class="btn btn-warning">
                                                    انتخاب این پاسخ به عنوان بهترین پاسخ
                                                </button>
                                                {!! Form::close() !!}
                                            </div>
                                        @endif
                                    @endcan
                                    <div class="row">
                                        {!! $answer->content !!}
                                        <br>
                                        @can('edit-answer', $answer)
                                            <span class="edit"><a
                                                        href="{{ route('answer.edit', $answer->id) }}">ویرایش</a></span>
                                        @endcan
                                    </div>
                                    @if(count($answer->comments))
                                        <div class="row allcomment">
                                            <div class="row">
                                                دیدگاه ها:
                                            </div>
                                            @foreach($answer->comments as $comment)
                                                <div class="comment">
                                                    {{ $comment->name }}
                                                    :
                                                    {{ $comment->content }}
                                                    @can('edit-comment', $comment)
                                                        <span class="edit"><a
                                                                    href="{{ route('comment.edit', $comment->id) }}">ویرایش</a></span>
                                                    @endcan
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                                <div class="col-sm-1">
                                    <div class="row">
                                        {!! Form::open(['route'=>['answer.positive', $answer->id ], 'method' => 'patch' ]) !!}
                                        <button type="submit" class="btn btn-success">
                                            <span>
                                                &nbspتشکر&nbsp
                                            </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>
                                    <div>
                                        {{ $answer->vote }}
                                        @if($answer->vote >= 0) + @endif
                                    </div>
                                    <div class="row">
                                        {!! Form::open(['route'=>['answer.negative', $answer->id ], 'method' => 'patch' ]) !!}
                                        <button type="submit" class="btn btn-danger">
                                            <span>
                                        گزارش
                                            </span>
                                        </button>
                                        {!! Form::close() !!}
                                    </div>

                                    <div class="row">

                                        @if(Auth::check())
                                            <span>
                                                <span>
                                                    نام:
                                                </span>
                                                <span>
                                                    {{ auth()->user()->name }}
                                                </span>
                                            </span>
                                            <span>/</span>
                                            <span>
                                                <a href="#"
                                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    خروج
                                                </a>
                                                <form id="logout-form" action="/logout" method="POST"
                                                      style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </span>
                                            {!! Form::open(['route'=>['comment.store', 'answer_id' => $answer->id ] ]) !!}
                                        @else
                                            {!! Form::open(['route'=>['comment.store', 'answer_id' => $answer->id ] ]) !!}
                                        <!-- فیلد نام شما: -->
                                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                {!! Form::label('name', 'نام شما:') !!}
                                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                                @if ($errors->has('name'))
                                                    <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                                                @endif
                                            </div>
                                            <div>
                                                <span>
                                                    <a href="{{ route('register') }}">
                                    ثبت نام
                                                    </a>
                                                </span>
                                                <span>/</span>
                                                <span>
                                                    <a href="{{ route('login') }}">
                                    ورود
                                                    </a>
                                                </span>
                                            </div>
                                        @endif
                                    <!-- فیلد دیدگاه -->
                                        <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                                            {!! Form::label('content', 'دیدگاه:') !!}
                                            {!! Form::text('content', null, ['class' => 'form-control']) !!}
                                            @if ($errors->has('content'))
                                                <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
                                            @endif
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn-warning">
                                                ثبت <br> دیدگاه
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                {{--end answers--}}

                {{--start answer form--}}
                <div class="row answerform">
                    @if(Auth::check())
                        <div>
                            <span>
                                وارد شده با نام کاربری:
                            </span>
                            <span>{{ auth()->user()->name }}</span>
                            <span>/</span>
                            <span>
                            <a href="#"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            خروج
                            </a>
                            <form id="logout-form" action="/logout" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            </span>
                        </div>
                        {!! Form::open(['route'=>['answer.store']]) !!}
                    @else
                        {!! Form::open(['route'=>['answer.store']]) !!}
                    <!-- فیلد نام شما: -->
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', 'نام شما:') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('name'))
                                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                            @endif
                        </div>
                        <span>
                                <a href="{{ route('register') }}">
                                ثبت نام
                                </a>
                        </span>
                        <span>/</span>
                        <span>
                                <a href="{{ route('login') }}">
                                ورود
                                </a>
                        </span>
                    @endif
                <!-- فیلد جواب شما: -->
                    <div id="answerthequestion" class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                        {!! Form::label('content', 'جواب شما:') !!}
                        {!! Form::textarea('content', null, ['class' => 'form-control', 'id' => 'editor1']) !!}
                        @if ($errors->has('content'))
                            <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
                        @endif
                    </div>

                    <input type="hidden" name="question_id" value="{{$question->id}}">
                    <button type="submit" class="btn btn-success"> ارسال جواب</button>

                    {!! Form::close() !!}
                </div>
                {{--end answer form--}}

            </div>

            {{--start left menu--}}
            {{--<div class="col-sm-2" style="background-color: blue">--}}
            {{--@include('layouts.leftmenu')--}}
            {{--</div>--}}
            {{--end left menu--}}

        </div>
    </div>

@endsection
@push('footer')
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ asset('editor/full/ckeditor/adapters/jquery.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('textarea#editor1').ckeditor();
        });
    </script>

@endpush