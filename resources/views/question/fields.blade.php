<!-- فیلد دسته -->
<div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'دسته') !!}
    {!! Form::select('category_id', $categories, null, ['class' => 'form-control']) !!}
    @if ($errors->has('category_id'))
        <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
    @endif
</div>
<!-- فیلد عنوان -->
<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
    {!! Form::label('title', 'عنوان') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
    @if ($errors->has('title'))
        <span class="help-block"><strong>{{ $errors->first('title') }}</strong></span>
    @endif
</div>
<!-- فیلد محتوا -->
<div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
    {!! Form::label('content', 'محتوا') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control', 'id'=>'editor1']) !!}
    @if ($errors->has('content'))
        <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
    @endif
</div>
<!-- فیلد برچسب ها -->
<div class="form-group {{ $errors->has('tag_id[]') ? ' has-error' : '' }}">
    {!! Form::label('tag_id[]', 'برچسب ها') !!}
    {!! Form::select('tag_id[]',$tags , (isset($question))?$question->tags:null, ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
    @if ($errors->has('tag_id[]'))
        <span class="help-block"><strong>{{ $errors->first('tag_id[]') }}</strong></span>
    @endif
</div>

<button type="submit" class="btn btn-success">ثبت</button>
<a href="{{ route('question.index') }}" class="btn btn-primary">صفحه نخست</a>