@extends('layouts.layout')
@section('content')

    <a href="{{ route('question.create') }}" class="btn-primary btn">سوال جدید</a>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>نویسنده</th>
            <th>عنوان</th>
            <th>محتوا</th>
            <th>دسته</th>
            <th>برچسب ها</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($questions as $question)
            <tr>
                <td>{{ $question->name }}</td>
                <td><a href="{{ route('question.edit', $question->id) }}">{{ $question->title }}</a></td>
                <td style="max-width: 500px">{{ $question->content }}</td>
                <td>
                    <a href="{{ route('category.edit', $question->category->id) }}"> {{ $question->category->name }} </a>
                </td>
                @if(count($question->tags))
                    <td style="max-width: 200px">
                        @foreach($question->tags as $tag)
                            <a href="{{ route('tag.edit', $tag->id) }}"><span> {{ $tag->name }}</span></a>
                            <span> , </span>
                        @endforeach
                    </td>
                @else
                    <td> -</td>
                @endif

                <td>
                    {!! Form::open(['route' => ['question.destroy', $question->id], 'method' => 'delete']) !!}
                    <div class="btn-group">
                        <a href="{!! route('question.show', [$question->id]) !!}" class="btn btn-default btn-xs"><i
                                    class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('question.edit', [$question->id]) !!}" class="btn btn-default btn-xs"><i
                                    class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>




@endsection
