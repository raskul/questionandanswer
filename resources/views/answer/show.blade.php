@extends('layouts.layout')
@section('content')

    <div class="col-sm-12">
        <div class="row">

            {{--start body--}}
            <div class="col-sm-10">

                {{--start answer--}}
                <div class="row">
                    <div class="col-sm-2">
                        <div class="row">
                            picture
                        </div>
                        <div class="row">
                            {{ $answer->name }}
                        </div>
                        <div class="row">
                            vote{{ $answer->vote }}
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                {{ $answer->created_at }}
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="row">
                            {{ $answer->content }}
                        </div>
                    </div>
                </div>
                {{--end answer--}}

                {{--start comments--}}
                <div class="row">
                    <div class="col-sm-2">
                        <div class="row">
                            picture
                        </div>
                        <div class="row">
                            name
                        </div>
                        <div class="row">
                            vote
                        </div>
                        <div class="row">
                            date
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <div class="row">
                            mante didgah
                        </div>
                    </div>
                </div>
                {{--end comments--}}

            </div>
            {{--end body--}}

            {{--start left menu--}}
            <div class="col-sm-2">
                <div class="row">
                    menu
                </div>
            </div>
            {{--end left menu--}}

        </div>
    </div>

@endsection
