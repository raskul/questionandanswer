@extends('layouts.layout')
@section('content')

    {!! Form::model($answer, ['route'=>['answer.update', $answer->id ], 'method' => 'patch' ]) !!}

    @include('answer.fields')

    <button type="submit" class="btn btn-success"> ویرایش جواب</button>

    {!! Form::close() !!}

@endsection
