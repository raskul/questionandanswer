@extends('layouts.layout')
@section('content')

    {!! Form::open(['route'=>['answer.store']]) !!}

    @include('answer.fields')

    <button type="submit" class="btn btn-success"> ارسال جواب </button>

    {!! Form::close() !!}

@endsection
