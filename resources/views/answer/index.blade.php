@extends('layouts.layout')
@section('title', 'جواب - صفحه اصلی')
@section('content')

    <table class="table table-hover">
        <thead>
        <tr>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($answers as $answer)
        <tr>
            <td>{{ $answer->content }}</td>
            <td>
            	{!! Form::open(['route' => ['answer.destroy', $answer->id], 'method' => 'delete']) !!}
            	<div class="btn-group">
            		<a href="{!! route('answer.show', [$answer->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
            		<a href="{!! route('answer.edit', [$answer->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
            		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            	</div>
            	{!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

@endsection
