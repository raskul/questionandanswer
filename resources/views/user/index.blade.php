@extends('layouts.layout')
@section('content')
    <a href="{{ route('user.create') }}" class="btn-primary btn">ساخت یوزر جدید</a>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>نام دسته</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>
            	{!! Form::open(['route' => ['user.destroy', $user->id], 'method' => 'delete']) !!}
            	<div class="btn-group">
            		<a href="{!! route('user.show', [$user->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
            		<a href="{!! route('user.edit', [$user->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
            		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            	</div>
            	{!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

@endsection
