@extends('layouts.layout')
@section('content')

    {!! Form::model($user, ['route'=>['user.update', $user->id ], 'method' => 'patch', 'files' => true ]) !!}

    @include('user.fields')

    {!! Form::close() !!}

@endsection
