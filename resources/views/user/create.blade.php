@extends('layouts.layout')
@section('content')

    {!! Form::open(['route'=>['user.store'], 'files' => true ]) !!}

    @include('user.fields')

    {!! Form::close() !!}

@endsection
