@extends('layouts.layout')
@section('content')

<div class="col-sm-12">
    <div class="row" style="margin-top: 50px">
        <div class="col-sm-4 col-sm-offset-1">
            <div class="row">
                <img src="{{route('home')}}/{{$user->picture}}" alt="" width="100%" height="100%">
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1">
            <div class="row">
                <span>
                    نام:
                </span>
                <span>{{ $user->name }}</span>
            </div>
            <div class="row">
                <span>
                    ایمیل:
                </span>
                <span>{{ $user->email }}</span>
            </div>
            <div class="row">
                <span>
                    امتیاز:
                </span>
                <span>{{ $user->vote }}</span>
            </div>
            <div class="row">
                درباره من :
            </div>
            <div class="row">{{ $user->profile }}</div>

        </div>
        <div class="col-sm-1">
            @can('edit-user', $user)
                <a href="{{route('user.edit', $user->id)}}" class="btn btn-primary">
                    ویرایش
                    <br>
                    پروفایل
                </a>
            @endcan
        </div>
    </div>
</div>

@endsection