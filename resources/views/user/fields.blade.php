<!-- فیلد نام -->
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'نام') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @if ($errors->has('name'))
        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
    @endif
</div>

<!-- فیلد ایمیل -->
<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    {!! Form::label('email', 'ایمیل') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
    @if ($errors->has('email'))
        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
</div>

<!-- فیلد رمز -->
<div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
{!! Form::label('password', 'رمز') !!}
{!! Form::password('password', ['class' => 'form-control']) !!}
@if ($errors->has('password'))
<span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
@endif
</div>

<!-- فیلد متن معرفی شما به دیگران : -->
<div class="form-group {{ $errors->has('profile') ? ' has-error' : '' }}">
    {!! Form::label('profile', 'متن معرفی شما به دیگران ') !!}
    {!! Form::textarea('profile', null, ['class' => 'form-control']) !!}
    @if ($errors->has('profile'))
        <span class="help-block"><strong>{{ $errors->first('profile') }}</strong></span>
    @endif
</div>

<!-- فیلد عکس -->
<div class="form-group {{ $errors->has('picture') ? ' has-error' : '' }}">
    {!! Form::label('picture', 'عکس') !!}
    {!!  Form::file('picture') !!}
    @if ($errors->has('picture'))
        <span class="help-block"><strong>{{ $errors->first('picture') }}</strong></span>
    @endif
</div>

<button type="submit" class="btn btn-success">ثبت</button>
<a href="{{ route('user.index') }}" class="btn btn-primary">صفحه نخست</a>