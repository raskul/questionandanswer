@extends('layouts.layout')
@section('title', 'پرسش و پاسخ')
@section('content')

    @include('layouts.header')

    <div class="col-sm-10 col-sm-offset-1">
        <div class="row" style="padding: 30px">
            <a href="{{ route('question.create') }}" class="btn-primary btn">سوال بپرسید</a>
        </div>
        @foreach($questions as $question)
            <div class="row" style="padding: 10px;margin: 10px;border: 3px dashed gainsboro">
                <div class="col-sm-1" style="border-left: 1px dotted grey">
                    {{ $question->name }}
                </div>
                <div class="col-sm-11">
                    <a href="{{ route('question.show', $question->id) }}"><strong>{{ $question->title }}</strong></a>
                    <div style="font-size: x-small;color: #545454;">{{ $question->content }}</div>
                </div>
            </div>
        @endforeach
    </div>

    @include('layouts.footer')
@endsection