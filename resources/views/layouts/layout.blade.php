<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="سوال و جواب">
    <meta name="author" content="Sadegh">
    <title>@yield('title')</title>
    <title>سوال و جواب</title>

    <!-- Bootstrap -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/bootstrap-rtl/dist/css/bootstrap-rtl.min.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('iransans/css/fontiran.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    @stack('header')

    <style>
        html, body {
            font-family: iransans, IRANSans, yekan, Tahoma;
        }
    </style>

</head>
<body>
<div class="container-fluid">
    <div class="row ">

        @yield('content')

    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
    $(".select2").select2({
        dir: "rtl",
        language: "fa",
        width: '100%',
        placeholder: "یکی را انتخاب کنید...",
        tags: false
    });
</script>

<script>$(window).load(function() {
        $("img").removeClass("preload");
    });</script>

@stack('footer')
</body>
</html>
