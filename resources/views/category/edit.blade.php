@extends('layouts.layout')
@section('content')

    {!! Form::model($category, ['route'=>['category.update', $category->id ], 'method' => 'patch' ]) !!}

    @include('category.fields')

    {!! Form::close() !!}

@endsection
