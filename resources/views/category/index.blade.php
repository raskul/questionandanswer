@extends('layouts.layout')
@section('content')
    <a href="{{ route('category.create') }}" class="btn-primary btn">ساخت دسته جدید</a>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>نام دسته</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
        <tr>
            <td>{{ $category->name }}</td>
            <td>
            	{!! Form::open(['route' => ['category.destroy', $category->id], 'method' => 'delete']) !!}
            	<div class="btn-group">
            		<a href="{!! route('category.show', [$category->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
            		<a href="{!! route('category.edit', [$category->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
            		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            	</div>
            	{!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

@endsection
