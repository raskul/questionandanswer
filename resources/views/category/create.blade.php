@extends('layouts.layout')
@section('content')

    {!! Form::open(['route'=>['category.store']]) !!}

    @include('category.fields')

    {!! Form::close() !!}

@endsection
