<!-- فیلد نام دسته -->
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {!! Form::label('name', 'نام دسته') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
    @if ($errors->has('name'))
        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
    @endif
</div>

<button type="submit" class="btn btn-success">ثبت</button>
<a href="{{ route('category.index') }}"class="btn btn-primary">صفحه نخست</a>