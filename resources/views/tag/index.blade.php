@extends('layouts.layout')
@section('content')

    <table class="table table-hover">
        <thead>
        <tr>
            <th>نام برچسب</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($tags as $tag)
        <tr>
            <td>{{ $tag->name }}</td>
            <td>
            	{!! Form::open(['route' => ['tag.destroy', $tag->id], 'method' => 'delete']) !!}
            	<div class="btn-group">
            		<a href="{!! route('tag.show', [$tag->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
            		<a href="{!! route('tag.edit', [$tag->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
            		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
            	</div>
            	{!! Form::close() !!}
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
	
@endsection
