@extends('layouts.layout')
@section('content')

    {!! Form::model($tag, ['route'=>['tag.update', $tag->id ], 'method' => 'patch' ]) !!}

{{--    <input type="hidden" name="question_id" value="{{ dd($tag->questions['question_id'] ) }}">--}}

    @include('tag.field')

    <button type="submit" class="btn btn-success"> ویرایش برچسب </button>

    {!! Form::close() !!}

@endsection
