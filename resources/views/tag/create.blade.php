@extends('layouts.layout')
@section('content')

    {!! Form::open(['route'=>['tag.store']]) !!}

    @include('tag.field')

    <button type="submit" class="btn btn-success">ارسال برچسب</button>

    {!! Form::close() !!}

@endsection
