@extends('layouts.layout')
@section('content')

    {!! Form::model($comment, ['route'=>['comment.update', $comment->id ], 'method' => 'patch' ]) !!}

    @include('comment.fields')

    {!! Form::close() !!}

@endsection
