<!-- فیلد نام شما: -->
<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'نام شما:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
        @endif
</div>

<!-- فیلد محتوا دیدگاه -->
<div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
    {!! Form::label('content', 'دیدگاه') !!}
    {!! Form::text('content', null, ['class' => 'form-control']) !!}
    @if ($errors->has('content'))
        <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
    @endif
</div>

<button class="btn btn-success" type="submit">ثبت</button>