@extends('layouts.layout')
@section('content')
    <a href="{{ route('comment.create') }}" class="btn-primary btn">دیدگاه جدید</a>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>نام</th>
            <th>متن</th>
            <th>عمیات</th>
        </tr>
        </thead>
        <tbody>
        @foreach($comments as $comment)
            <tr>
                <td>{{ $comment->name }}</td>
                <td>{{ $comment->content }}</td>
                <td>
                	{!! Form::open(['route' => ['comment.destroy', $comment->id], 'method' => 'delete']) !!}
                	<div class="btn-group">
                		<a href="{!! route('comment.show', [$comment->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
                		<a href="{!! route('comment.edit', [$comment->id]) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-edit"></i></a>
                		{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                	</div>
                	{!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
